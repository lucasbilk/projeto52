
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {

    public static void main(String[] args) {
        Equacao2Grau<Integer> teste1 = new Equacao2Grau<>(3, 5, 2);
        Equacao2Grau<Integer> teste2 = new Equacao2Grau<>(9, 12, 4);
        Equacao2Grau<Integer> teste3 = new Equacao2Grau<>(5, 2, 1);

        try {
            System.out.println("F1");
            System.out.println("Equacao 1 raiz 1: " + teste1.getRaiz1());
            System.out.println("Equacao 1 raiz 2: " + teste1.getRaiz2());

        } catch (Exception e) {
            System.out.println("Erro: " + e);
        } finally {
            try {
                System.out.println("F2");
                System.out.println("Equacao 2 raiz 1: " + teste2.getRaiz1());
                System.out.println("Equacao 2 raiz 2: " + teste2.getRaiz2());

            } catch (Exception e) {
                System.out.println("Erro: " + e);
            } finally {
                try {
                    System.out.println("F3");
                    System.out.println("Equacao 3 raiz 1: " + teste3.getRaiz1());
                    System.out.println("Equacao 3 raiz 2: " + teste3.getRaiz2());
                } catch (Exception e) {
                    System.out.println("Erro: " + e);
                }
            }
        }

    }
}
